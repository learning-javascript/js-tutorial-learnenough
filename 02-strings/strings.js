console.log('It\'s not easy being green'); // single quotes
console.log("It's not easy being green"); // double quotes

console.log('foo' + 'bar'); // string concatenation

const firstName = 'Edu';
const lastName = 'Finn';
console.log(firstName + ' ' + lastName);

// string interpolation
console.log(`${firstName} ${lastName}`);

// printing
console.log(firstName, lastName);

// Properties, booleans, and control flow
console.log('badger'.length);
console.log(''.length);

console.log('badger'.length > 3);
console.log('badger'.length > 6);
console.log('badger'.length >= 6);
console.log('badger'.length < 10);
console.log('badger'.length === 6);
console.log();

// difference between comparison operators
console.log("'1' == 1", '1' == 1);
console.log("'1' === 1", '1' === 1);
console.log();

let password = 'foo';
if (password.length < 6) {
    console.log('Password is too short.');
}

password = 'foobar';
if (password.length < 6) {
    console.log('Password is too short.');
} else {
    console.log('Password is long enough.');
}
console.log();

// Combining and inverting booleans
console.log('true && true =', true && true);
console.log('false && true =', false && true);
console.log('true && false =', true && false);
console.log('false && false =', false && false);

const x = 'foo';
const y = '';
if (x.length === 0 && y.length === 0) {
    console.log('Both strings are empty!');
} else {
    console.log('At least one of the strings is nonempty.');
}
console.log();

console.log('true || true =', true || true);
console.log('false || true =', false || true);
console.log('true || false =', true || false);
console.log('false || false =', false || false);
if (x.length === 0 || y.length === 0) {
    console.log('At least one of the strings is empty!');
} else {
    console.log('Neither of the strings is empty.');
}
console.log();

console.log('!true =', !true);
console.log('!false =', !false);
if (!(x.length === 0)) {
    console.log('x is not empty.');
} else {
    console.log('x is empty.');
}

console.log('!(x.length === 0) =', !(x.length === 0));
if (x.length !== 0) {
    console.log('x is not empty.');
} else {
    console.log('x is empty.');
}
console.log();

// Bang Bang
console.log('!!true =', !!true);
console.log('!!false =', !!false);

console.log('!!"foo" =', !!'foo');
console.log('!!"" =', !!'');

if (!x && !y) {
    console.log('Both strings are empty!');
} else {
    console.log('At least one of the strings is nonempty.');
}
console.log();

// Methods
console.log('HONEY BADGER'.toLocaleLowerCase());

const username = firstName.toLocaleLowerCase();
console.log(`${username}@example.com`);
console.log(lastName.toUpperCase());

const soliloquy = "To be, or not to be, that is the question:";
console.log(soliloquy.includes("To be"));
console.log(soliloquy.includes("question"));
console.log(soliloquy.includes("nonexistent"));
console.log(soliloquy.includes("TO BE"));
console.log(soliloquy.includes("To be", 1));
console.log(soliloquy.includes("o be,", 1));
console.log();

// String iteration
console.log(soliloquy);
console.log(soliloquy.charAt(0));
console.log(soliloquy.charAt(1));
console.log(soliloquy.charAt(2));
console.log();

for (let i = 0; i < 5; i++) {
    console.log(i);
}

console.log(soliloquy.length);
for (let i = 0; i < 42; i++) {
    console.log(soliloquy.charAt(i));
}
console.log();

for (let i = 0; i < soliloquy.length; i++) {
    console.log(soliloquy.charAt(i));
}
