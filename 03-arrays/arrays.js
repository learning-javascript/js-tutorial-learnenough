// Splitting
console.log("ant bat cat".split(" "));
console.log("ant,bat,cat".split(","));
console.log("ant, bat, cat".split(", "));
console.log("antheybatheycat".split("hey"));
console.log("badger".split(""));
console.log();

// Array access
let a = "badger".split("");
console.log(a[0]);
console.log(a[1]);
console.log(a[2]);

console.log('badger'[0]);
console.log('badger'[1]);
console.log();

const soliloquy = "To be, or not to be, that is the question:";
a = ["badger", 42, soliloquy.includes("To be")];
console.log(a);
console.log(a[2]);
console.log(a[3], !!a[3]);
console.log();

// Array slicing
a = [42, 8, 17, 99];
console.log(a.slice(1));
console.log(a.slice(1, 3));

console.log(a.length);
console.log(a[a.length - 1]);

const aMuchLongerArrayName = a;
console.log(aMuchLongerArrayName[aMuchLongerArrayName.length - 1]);
console.log(aMuchLongerArrayName.slice(-1));
console.log(aMuchLongerArrayName.slice(-1)[0]);
console.log();

// More array methods
console.log(a);
console.log(a.includes(42));
console.log(a.includes('foo'));
console.log();

// Sorting and reversing
console.log('unsorted', a);
console.log('sorted', a.sort());
console.log('reversed', a.reverse());
console.log();

// Pushing and popping
console.log(a.push(6));
console.log('after push', a);
console.log(a.push("foo"));
console.log('after another push', a);
console.log(a.pop());
console.log(a.pop());
console.log('after 2 pops', a);

const lastElement = a.pop();
console.log(lastElement);
console.log(a);

const theAnswerToLifeTheUniverseAndEverything = a.pop();
console.log(theAnswerToLifeTheUniverseAndEverything);
console.log();

// Undoing a split
a = ["ant", "bat", "cat", 42];
console.log(a);
console.log(a.join());
console.log(a.join(', '));
console.log(a.join(' -- '));
console.log(a.join(''));
console.log();

// Array iteration
for (let i = 0; i < a.length; i++) {
  console.log(a[i]);
}
console.log();
