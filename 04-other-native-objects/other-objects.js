// Math and Number
console.log('1 + 1 =', 1 + 1);
console.log('2 - 3 =', 2 - 3);
console.log('2 * 3 =', 2 * 3);
console.log('10 / 5 =', 10 / 5);
console.log('2 / 3 =', 2 / 3);
console.log();

// More advanced operations
console.log('Math.PI', Math.PI);
console.log('Math.pow(2, 3)', Math.pow(2, 3));
console.log('Math.sqrt(3)', Math.sqrt(3));
console.log('Math.cos(2*Math.PI)', Math.cos(2*Math.PI));
console.log('Math.E', Math.E);
console.log(Math.log(Math.E));
console.log('Math.log(10)', Math.log(10));
console.log('Math.log10(10)', Math.log10(10));
console.log('Math.log10(1000000)', Math.log10(1000000));
console.log('Math.log10(Math.E)', Math.log10(Math.E));
console.log();

// Math to string
const tau = 2 * Math.PI;
console.log(tau.toString());
// console.log(100.toString()); // this cause an error
console.log(100.0.toString()); // this works
console.log(String(100.0));
console.log(String(tau));
console.log();

console.log('Number("6.283185307179586") =', Number("6.283185307179586"));
console.log('String(Number("6.283185307179586")) =', String(Number("6.283185307179586")));
console.log('Number(\'1.24e6\') =', Number('1.24e6'));
console.log('1.24e6 = ', 1.24e6);
console.log();

// Dates
let s = new String("A man, a plan, a canal—Panama!");
console.log(s);
console.log(s.split(", "));

let a = new Array();
console.log(a.push(3));
console.log(a.push(4));
console.log(a.push("hello, world!"));
console.log(a);
console.log(a.pop());
console.log();

const now = new Date();
console.log(now);

const moonLanding = new Date("July 20, 1969 20:18");
console.log(now - moonLanding);
console.log(now.getYear());
console.log(now.getFullYear());
console.log(now.getMonth());
console.log(now.getDay());
console.log();

const daysOfTheWeek = ["Sunday", "Monday", "Tuesday", "Wednesday",
  "Thursday", "Friday", "Saturday"];
console.log(daysOfTheWeek[now.getDay()]);
console.log();

// Regular expressions
let zipCode = new RegExp("\\d{5}");
const result = zipCode.exec('Beverly Hills 90210');
console.log(result);
console.log(result.length);
console.log();

zipCode = /\d{5}/;
s = "Beverly Hills 90210 was a '90s TV show set in Los Angeles.";
s += " 91125 is another ZIP code in the Los Angeles area.";
console.log(s.match(zipCode));
console.log('!!s.match(zipCode) =', !!s.match(zipCode));
if (s.match(zipCode)) {
  console.log("Looks like there's at least one ZIP code in the string!");
}

zipCode = /\d{5}/g;    // Use 'g' to set the 'global' flag
console.log(s.match(zipCode));
console.log();

console.log("ant bat cat duck".split(" "));
console.log("ant bat cat duck".split(/\s+/));
console.log("ant    bat\tcat\nduck".split(/\s+/));
console.log();
const sonnet = `Let me not to the marriage of true minds
Admit impediments. Love is not love
Which alters when it alteration finds,
Or bends with the remover to remove.
O no, it is an ever-fixed mark
That looks on tempests and is never shaken;
It is the star to every wand'ring bark,
Whose worth's unknown, although his height be taken.
Love's not time's fool, though rosy lips and cheeks
Within his bending sickle's compass come:
Love alters not with his brief hours and weeks,
But bears it out even to the edge of doom.
  If this be error and upon me proved,
  I never writ, nor no man ever loved.`;

// Plain objects
const user = {};
user['firstName'] = 'Edu';
user['lastName'] = 'Finn';
console.log(user['firstName']);
console.log(user['lastName']);
console.log(user.firstName);
console.log(user.lastName);
console.log(user['dude']);
console.log(user.dude);
console.log(!!user.dude);
console.log();

console.log(user);
const otherUser = { firstName: 'Foo', lastName: 'Bar' };
console.log(otherUser.firstName);
console.log(otherUser.lastName);
console.log();

// Map
const uniques = new Map();
uniques.set('loved', 0);
console.log(uniques);

const currentValue = uniques.get('loved');
uniques.set('loved', currentValue + 1);
console.log(uniques);
console.log();
console.log();
console.log();
