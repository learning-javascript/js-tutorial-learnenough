let a = ['ant', 'bat', 'cat', 42];
a.forEach(function(element) {
    console.log(element);
});

const soliloquy = 'To be, or not to be, that is the question:';
Array.from(soliloquy).forEach(function(character) {
  console.log(character);
});
console.log('=====');

Array.from(soliloquy).forEach(character => {
    console.log(character);
});

// exercises
a = [17, 8, 99, 42];
console.log(a);
console.log(a.sort(function(a, b) { return a - b; }));
console.log(a.sort((a, b) => a - b));
a.sort((a, b) => a - b).forEach(el => {
    console.log(el);
});
