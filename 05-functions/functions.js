console.log('hello, world!');

// function definitions
function stringMessage(string) {
    if (string) {
        return 'The string is nonempty.';
    } else {
        return "It's an empty string!";
    }
}

console.log(stringMessage('honey badger'));
console.log(stringMessage(''));
console.log();

// Sorting numerical arrays
const a = [8, 17, 42, 99];
console.log(a.sort());

function numberCompare(a, b) {
    if (a > b) {
        return 1;
    } else if (a < b) {
        return -1;
    } else {
        return 0;
    }
}

console.log(numberCompare(8, 17));
console.log(numberCompare(17, 99));
console.log(numberCompare(99, 42));
console.log(numberCompare(99, 99));
console.log(a.sort(numberCompare));
console.log();

// Fat arrow
const altStringMessage = (string) => {
    if (string) {
        return "The string is nonempty.";
    } else {
        return "It's an empty string!";
    }
}
console.log(altStringMessage('honey badger'));
console.log(altStringMessage(''));
console.log();

// exercise
function square(x) {
    return x * x;
}
const altSquare = (x) => x * x;
console.log(square(2));
console.log(altSquare(2));
console.log();

// Method chaining
console.log(a);
console.log(a.reverse());

console.log('racecar'.split(''));
console.log([ 'r', 'a', 'c', 'e', 'c', 'a', 'r' ].join(''));
console.log();

const string = 'Racecar';
console.log(string.split('').reverse().join(''));

console.log(string === string.split('').reverse().join(''));
console.log();

const processedContent = string.toLowerCase();
console.log(processedContent);
console.log(processedContent === processedContent.split('').reverse().join(''));
console.log();

// emoji
console.log(Array.from('honey badger'));
console.log();

// Interation for each
[42, 17, 85].forEach(function(element) {
    console.log(element);
});
console.log();
console.log();
console.log();
console.log();
console.log();
