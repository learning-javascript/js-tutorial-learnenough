// Reverses a string.
function reverse(string) {
    return Array.from(string).reverse().join('');
}

// Returns true for a palindrome, false otherwise.
function palindrome(string) {
    const processedContent = string.toLowerCase();
    return processedContent === reverse(processedContent);
}

console.log(palindrome('To be or not to be'));
console.log(palindrome('Racecar'));
console.log(palindrome('level'));
