const cantons = ['Zurich', 'Bern', 'Appenzell Innerrhoden', 'Appenzell Ausserrhoden'];
// url: imperative version
function imperativeUrls(elements) {
    let urls = [];
    elements.forEach(function(element) {
        urls.push(element.toLowerCase().split(/\s+/).join('-'));
    });
    return urls;
}
console.log(imperativeUrls(cantons));
console.log();

// map function
console.log([1, 2, 3, 4].map(function(n) { return n * n; })); // using function
console.log([1, 2, 3, 4].map(n => n * n)); // using fat arrow
console.log();

// urls: functional version
function functionalUrls(elements) {
    return elements.map(element => element.toLowerCase().split(/\s+/).join('-'));
}
console.log(functionalUrls(cantons));
console.log();

// Returns a URL-friendly version of a string.
function urlify(string) {
    return string.toLowerCase().split(/\s+/).join('-');
}

// url: imperative version 2
function imperativeUrls2(elements) {
    let urls = [];
    elements.forEach(function(element) {
        urls.push(urlify(element));
    });
    return urls;
}
console.log(imperativeUrls2(cantons));

// urls: functional version 2
function functionalUrls2(elements) {
    return elements.map(urlify);
}
console.log(functionalUrls2(cantons));
console.log();

// singles: Imperative version
function imperativeSingles(elements) {
    let singles = [];
    elements.forEach(function(element) {
        if (element.split(/\s+/).length === 1) {
            singles.push(element);
        }
    });
    return singles;
}
console.log(imperativeSingles(cantons));
console.log();

// filter function
console.log(16 % 2); // even
console.log(17 % 2); // odd
console.log(16 % 2 === 0); // even
console.log(17 % 2 === 0); // odd
console.log([1, 2, 3, 4, 5, 6, 7, 8].filter(n => n % 2 === 0));
console.log(cantons.filter(state => state.split(/\s+/).length === 1));
console.log();

// singles: Functional version
function functionalSingles(elements) {
    return elements.filter(element => element.split(/\s+/).length === 1);
}
console.log(functionalSingles(cantons));
console.log();

// exercises
// check for Appenzell
function findAppenzell(elements) {
    return elements.filter(canton => canton.includes('Appenzell'));
}
console.log(findAppenzell(cantons));
console.log();

// check for 2 word
function checkForTwoWords(elements) {
    return elements.filter(element => element.split(/\s+/).length === 2);
}
console.log(checkForTwoWords(cantons));
console.log();

// reduce function
const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
// sum: Imperative solution
function imperativeSum(elements) {
    let total = 0;
    elements.forEach(function(n) {
        total += n;
    });
    return total;
}
console.log(imperativeSum(numbers));
console.log();

const result = numbers.reduce((total, n) => {
    total += n;
    return total;
}, 0);
console.log(result);
console.log(numbers.reduce((total, n) => { return total += n }, 0));
console.log(numbers.reduce((total, n) => total += n, 0));            // one line fat arrow doesn't need return
console.log(numbers.reduce((total, n) => { return total += n }));
console.log(numbers.reduce((total, n) => total += n));               // one line fat arrow doesn't need return
console.log();

// sum: Functional solution
function functionalSum(elements) {
    return elements.reduce((total, n) => total += n);
}
console.log(functionalSum(numbers));
console.log();

// lengths: Imperative solution
function imperativeLengths(elements) {
    let lengths = {};
    elements.forEach(function(element) {
        lengths[element] = element.length;
    });
    return lengths;
}
console.log(imperativeLengths(cantons));
console.log();

// lengths: Functional solution
function functionalLengths(elements) {
    return elements.reduce((lengths, element) => {
        lengths[element] = element.length;
        return lengths;
    }, {});
}
console.log(functionalLengths(cantons));
console.log();

// exercises
// product: functional
function functionalProduct(elements) {
    return elements.reduce((total, n) => total *= n);
}
console.log(functionalProduct(numbers));
console.log();

// lengths: Functional solution without new lines
function functionalLengthsNoNewlines(elements) {
    return elements.reduce((lengths, element) => {lengths[element] = element.length; return lengths;}, {});
}
console.log(functionalLengthsNoNewlines(cantons));
console.log();
