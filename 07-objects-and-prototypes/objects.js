// Defining objects
function Phrase(content) {
    this.content = content;
}

const greeting = new Phrase('Hello, world!');
console.log(greeting);
console.log(greeting.content);
console.log();

// Modifying native objects
String.prototype.reverse = function() {
    return Array.from(this).reverse().join('');
};
console.log('foobar'.reverse());
console.log('Racecar'.reverse());

const string = 'Able was I ere I saw Elba';
console.log(string.reverse());
console.log();

function PhraseAgain(content) {
    this.content = content;

    // Returns content processed for palindrome testing.
    this.processedContent = function processedContent() {
        return this.content.toLowerCase();
    };

    // Returns true if the phrase is a palindrome, false otherwise.
    this.palindrome = function palindrome() {
        return this.processedContent() === this.processedContent().reverse();
    }
}

const napoleonsLament = new PhraseAgain('Able was I ere I saw Elba');
console.log(napoleonsLament.palindrome());
console.log();

// exercises
String.prototype.blank = function () {
    return !!this.match(/^\s*$/);
};
console.log(''.blank());
console.log(' '.blank());
console.log('\t \n'.blank());
console.log();

Array.prototype.last = function() {
    return this[this.length - 1];
};
const array = ['ant', 'bat', 'cat', 42];
console.log(array);
console.log(array.last());

