// Reverses a string.
function reverse(string) {
    return Array.from(string).reverse().join("");
}

// Defines a Phrase object.
function Phrase(content) {
    this.content = content;

    // Returns content processed for palindrome testing.
    this.processedContent = function processedContent() {
        return this.content.toLowerCase();
    }

    // Returns true if the phrase is a palindrome, false otherwise.
    this.palindrome = function palindrome() {
        return this.processedContent() === reverse(this.processedContent());
    }

    // Makes the phrase LOUDER.
    this.louder = function() {
        return this.content.toUpperCase();
    };
}
const phrase = new Phrase('Racecar');
console.log(phrase.palindrome());
console.log(phrase.louder());
console.log();

// example of object extension by duplication
function TranslatedPhraseWithDuplication(content, translation) {
    this.content = content;
    this.translation = translation;
  
    // Returns content processed for palindrome testing.
    this.processedContent = function processedContent() {
        return this.content.toLowerCase();
    }
  
    // Returns true if the phrase is a palindrome, false otherwise.
    this.palindrome = function palindrome() {
        return this.processedContent() === reverse(this.processedContent());
    }
}

// better way to extend object by using prototye
// Defines a TranslatedPhrase object.
function TranslatedPhrase(content, translation) {
    this.content = content;
    this.translation = translation;
}
TranslatedPhrase.prototype = new Phrase();
const frase = new TranslatedPhrase('recognize', 'reconocer');
console.log(frase.palindrome());

function TranslatedPhraseTranslate(content, translation) {
    this.content = content;
    this.translation = translation;

    // Returns translation processed for palindrome testing.
    this.processedContent = function processedContent() {
        return this.translation.toLowerCase();
    }
}
TranslatedPhraseTranslate.prototype = new Phrase();
const frase2 = new TranslatedPhraseTranslate('recognize', 'reconocer');
console.log(frase2.palindrome());
