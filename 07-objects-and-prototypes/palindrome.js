// Reverses a string.
function reverse(string) {
    return Array.from(string).reverse().join("");
}

// Defines a Phrase object.
function Phrase(content) {
    this.content = content;

    // Returns true if the phrase is a palindrome, false otherwise.
    this.palindrome = function palindrome() {
        let processedContent = this.content.toLowerCase();
        return processedContent === reverse(processedContent);
    }

    // Makes the phrase LOUDER.
    this.louder = function() {
        return this.content.toUpperCase();
    };
}
const phrase = new Phrase('Racecar');
console.log(phrase.palindrome());
console.log(phrase.louder());
