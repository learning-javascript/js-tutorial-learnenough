const assert = require('assert');
const Phrase = require('../palindrome');

describe('Phrase', () => {
  describe('#palindrome', () => {
    it('should return false for a non-palindrome', () => {
      const nonPalindrome = new Phrase('apple');
      assert(!nonPalindrome.palindrome());
    });

    it('should return true for a plain palindrome', () => {
      const plainPalindrome = new Phrase('racecar');
      assert(plainPalindrome.palindrome());
    });

    it('should return true for a mixed-case palindrome', () => {
      const mixedCasePalindrome = new Phrase('RaceCar');
      assert(mixedCasePalindrome.palindrome());
    });

    it('should return true for a palindrome with punctuation', () => {
      const punctuatedPalindrome = new Phrase("Madam, I'm Adam.");
      assert(punctuatedPalindrome.palindrome());
    });
  });

  describe('#letters', () => {
    it('should return only letters', () => {
      const punctuatedPalindrome = new Phrase("Madam, I'm Adam.");
      assert.strictEqual(punctuatedPalindrome.letters(), `MadamImAdam`);
    });

    it('should return the empty string on no match', function() {
      let noLetters = new Phrase('1234.56');
      assert.strictEqual(noLetters.letters(), '');
    });
  });
});
