import Phrase from './palindrome/index.js';

function palindromeTester(event) {
    event.preventDefault();
    const phrase = new Phrase(event.target.phrase.value);
    const palindromeResult = document.querySelector("#palindromeResult");

    if (phrase.palindrome()) {
        palindromeResult.innerHTML = `"${phrase.content}" is a palindrome!`;
    } else {
        palindromeResult.innerHTML = `"${phrase.content}" is not a palindrome.`;
    }
}

document.addEventListener('DOMContentLoaded', () => {
    const button = document.querySelector('#palindromeTester');
    button.addEventListener('submit', (event) => {
        palindromeTester(event);
    });
});
