function activateGallery() {
  const thumbs = document.querySelectorAll('#gallery-thumbs > div > img');
  const mainImage = document.querySelector('#gallery-photo img');

  thumbs.forEach(thumb => {
    // Preload large images.
    const newImageSrc  = thumb.dataset.largeVersion;
    const newImageTitle  = thumb.dataset.title;
    const largeVersion = new Image();
    largeVersion.src = newImageSrc;

    thumb.addEventListener('click', () => {
      // Set clicked image as main image
      mainImage.setAttribute('src', newImageSrc);
      mainImage.setAttribute('alt', newImageTitle);

      // Change which image is current
      document.querySelector('.current').classList.remove('current');
      thumb.parentNode.classList.add('current');

      // Update image info
      const galleryInfo = document.querySelector('#gallery-info');
      const title = galleryInfo.querySelector('.title');
      const description = galleryInfo.querySelector('.description');

      title.innerHTML = newImageTitle;
      description.innerHTML = thumb.dataset.description;
    });
  });
}
